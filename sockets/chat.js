module.exports = function(io){

  var crypto = require('crypto')
    , onlines = {}
    , sockets = io.sockets;

  sockets.on('connection', function (client) {
    var session = client.handshake.session
      , user = session.user
    ;

    onlines[user.email] = user.email;
    for(var email in onlines){
      client.emit('notify-onlines', email);
      client.broadcast.emit('notify-onlines', email);
    }

    client.on("join", function(room){
      // if(room){
      //   room = room.replace('?','');
      // }else{
      //   var timestamp = new Date().toString();
      //   var md5 = crypto.createHash('md5');
      //   room = md5.update(timestamp).digest('hex');
      // }
      // client.set("room", room);
      // client.join(room);
      if(!room){
        var timestamp = new Date().toString()
          , md5 = crypto.createHash('md5');
        room = md5.update(timestamp).digest('hex');
      }
      session.room = room;
      client.join(room);

    }); // on - join

    client.on("disconnect", function(){
      // client.get("room", function(err, room){
      //   client.leave(room);
      // }); // get - room
      var room = session.room
        , msg = "<b>" + user.name + ": </b> leave. <br>";

      client.broadcast.emit('notify-offlines', user.email);
      sockets.in(room).emit('send-client', msg);
      delete onlines[user.email];
      client.leave(room);
    }); // on - disconnect

    client.on("send-server", function(msg){
        var room = session.room
          , data = {email: user.email, room: room};
        var msg = "<b>"+user.name+":</b> "+msg+"<br>";
        client.broadcast.emit("new-message", data);
        sockets.in(room).emit('send-client', msg);
    }); // on - send-server
  }); // on - connection
} // module
