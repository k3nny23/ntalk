exports.notFound = function(req, res, next){
    res.status(404);
    
    // respond with html page..
    if(req.accepts('html')){
      res.render('not-found');
      return;
    }

    // respond with json
    if(req.accepts('json')){
      res.send({error: 'Not found'});
      return;
    }
}

exports.serverError = function(err, req, res, next){
  res.status(500);
  res.render('server-error', {error: err});
}
