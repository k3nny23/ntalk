// express, this is upgrade of connect, then we need to use right order to put
// each item in this setup. http://www.senchalabs.org/connect

const KEY = 'ntalk.sid', SECRET = 'ntalk';
// first library
var express = require('express')
  , load = require('express-load')
  , bodyParser = require('body-parser')
  , cookieParser = require('cookie-parser')
  , expressSession = require('express-session')
  , methodOverride = require('method-override')
  , error = require('./middleware/error')
  , app = express()
  , server = require('http').createServer(app)
  , io = require('socket.io').listen(server)
  , cookie = cookieParser(SECRET)
  , store = new expressSession.MemoryStore()
  , mongoose = require('mongoose');

  global.db = mongoose.connect('mongodb://localhost:27017/ntalk');

// thrid using set() and use()
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

// be carefull for you override native field of session, for exemple
// req.session.destroy or regenate, this is dangerous for your application.
//http://www.senchalabs.org/connect/session.html
app.use(cookie);
app.use(expressSession({
  secret: SECRET,
  name: KEY,
  resave: true,
  saveUninitialized: true,
  store: store
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('_method'));
app.use(express.static(__dirname + '/public'));

// io.set('authorization', function(data, accept){
//   cookie(data, {}, function(err){
//     var sessionID = data.signedCookies[KEY];
//     store.get(sessionID, function(err, session){
//
//       if(err || !session){
//         accept(null, false);
//       } else {
//         data.session = session;
//         accept(null, true);
//       }
//     });
//   });
// });

io.use(function(socket, next){
  var data = socket.request;
  cookie(data, {}, function(err){
    var sessionID = data.signedCookies[KEY];
    store.get(sessionID, function(err, session){
      if(err || !session){
        return next(new Error('acesso negado'));
      }else{
        socket.handshake.session = session;
        return next();
      }
    });
  });
});


// load()...
load('models')
  .then('controllers')
  .then('routes')
  .into(app);
load('sockets')
  .into(io);

// you can find it in ./middleware/error.js
  app.use(error.notFound);
  // app.use(error.serverError);

// fiveth use listen()
server.listen(3000, function(){
    console.log("Ntalk server up.");
});
